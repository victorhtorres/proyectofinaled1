
package tf_edya1_2016_03_apellidos;

/**
 * Fecha de asignación: Cali, 3 de noviembre de 2016.
 * Fecha y hora máximas de entrega: viernes 11 de noviembre a las 8:00 PM, 
 * vía Moodle. Recuerden enviar el Diseño UML y colocar apellidos, nombres y
 * códigos en sus entregas.
 * 
 * @author Apellidos, Nombres y Códigos.
 * 
 */

public class MainMenu {

    public static void main(String[] args) {
        // ---------------------------------------------------------------------
        EmpresaCondominios_Menu menu = new EmpresaCondominios_Menu();
        // ---------------------------------------------------------------------
    }
    
}   // Fin de la clase MainMenu