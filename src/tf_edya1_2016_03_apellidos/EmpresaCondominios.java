package tf_edya1_2016_03_apellidos;

// DEBEN usar esta colección:
// *************************
import java.util.LinkedList;    // DEBEN usar esta colección.
import java.util.List;

/**
 * Fecha de asignación: Cali, 3 de noviembre de 2016. Fecha y hora máximas de
 * entrega: viernes 11 de noviembre a las 8:00 PM, vía Moodle. Recuerden enviar
 * el Diseño UML y colocar apellidos, nombres y códigos en sus entregas.
 *
 * @author Apellidos, Nombres y Códigos.
 *
 */
// Clase que modela la empresa administradora de los condominios:
public class EmpresaCondominios {

    // Atributos:
    private String RazonSocial; // Nombre de la empresa que administra los condominios.
    private String NIT; // NIT (Número de Identificación Tributario) de la empresa.
    // Arreglo dinámico de referencias a objetos de la clase Casa, que guarda
    // las casas de la empresa que administra los condominios. 
    private LinkedList<Casa> casas = new LinkedList<>();

    public String getRazonSocial() {
        return RazonSocial;
    }

    public void setRazonSocial(String RazonSocial) {
        this.RazonSocial = RazonSocial;
    }

    public String getNIT() {
        return NIT;
    }

    public void setNIT(String NIT) {
        this.NIT = NIT;
    }

    public LinkedList<Casa> getCasas() {
        return casas;
    }

    public void setCasas(Casa obj) {
        this.casas.add(obj);
    }

    public void reporteCasasMora() {
        StringBuilder cad = new StringBuilder();
        boolean hayCasaMora = false;
        cad.append("*** Reporte de casas en mora¨***\n");
        ordenamientoQS_Ascendente(casas);
        for (Casa iter : casas) {
            if (iter.getValorAdmon() > 250000) {
                cad.append(iter.toString()).append("\n");
                hayCasaMora = true;
            }

        }

        if (!hayCasaMora) {
            Util.imprimir("No hay casas en mora...", "¡Información Importante!", 1);
        } else {
            System.out.println(cad.toString());
        }

    }

    public void totalRecaudadoCasasOk() {
        int total = 0;
        for (Casa iter : casas) {
            if (iter.getValorAdmon() <= 250000) {
                total += iter.getValorAdmon();
            }
        }
        if (total == 0) {
            Util.imprimir("No se tiene casas al día ¡Todas están en mora!", "¡Información Importante!", 1);
        } else {

            System.out.println("Total recaudado de las casas al día: $" + total);

        }

    }

    public void mayorValorAdeudado() {
        double mayor = casas.get(0).getValorAdmon();
        Casa casa = casas.get(0);
        for (Casa iter : casas) {
            if (mayor < iter.getValorAdmon()) {
                mayor = iter.getValorAdmon();
                casa = iter;
            }
        }

        if (mayor > 250000) {
            System.out.println("la casa con mayor deuda en mora es: " + casa.toString());
        } else {
            Util.imprimir("No existe alguna casa en mora (> $250.000).", "¡Información Importante!", 1);
        }

    }

    public int busquedaBinariaRecursiva(int numCasa) {
        ordenamientoQS_Ascendente(casas);// La lista cuentas queda ordenada por números de cuenta.
        return busquedaBinariaRecursiva(numCasa, 0, casas.size() - 1, casas);
    }

    private int busquedaBinariaRecursiva(int numCta, int izq, int der, LinkedList<Casa> lista) {
        if (izq > der) {
            return -1;    // Caso base. Significa que numCta no está en lista.
        }
        int mitad = (izq + der) / 2;    // Variable local.
        if (numCta < lista.get(mitad).getNumero()) {
            return busquedaBinariaRecursiva(numCta, izq, mitad - 1, lista);   // Caso recursivo.
        } else if (numCta > lista.get(mitad).getNumero()) {
            return busquedaBinariaRecursiva(numCta, mitad + 1, der, lista);   // Caso recursivo.
        } else {
            return mitad;     // Caso base. Significa que x está en lista.
        }
    }

    public static void ordenamientoQS_Ascendente(List<Casa> elementos) {
        if (elementos.size() > 1) {
            // Crea 3 listas enlazadas vacías:
            List<Casa> izquierda = new LinkedList<>();
            List<Casa> iguales = new LinkedList<>();
            List<Casa> derecha = new LinkedList<>();
            // Que el elemento ubicado en la mitad sea el pivote:
            Integer pivote = elementos.get(elementos.size() / 2).getNumero();
            for (Casa i : elementos) {
                if (i.getNumero() < pivote) {
                    izquierda.add(i);
                } else if (i.getNumero() > pivote) {
                    derecha.add(i);
                } else {
                    iguales.add(i);
                }
            }   // Fin for

            ordenamientoQS_Ascendente(izquierda);    // Llamada recursiva.
            ordenamientoQS_Ascendente(derecha);    // Llamada recursiva.

            elementos.clear();  // Removes all of the elements from this list.

            // Appends all of the elements in the specified collection to the
            // end of this list:
            elementos.addAll(izquierda);
            elementos.addAll(iguales);
            elementos.addAll(derecha);

        }   // Fin if

    }   // Fin método ordenamientoQS_Ascendente(...)

    @Override
    public String toString() {
        return "EmpresaCondominios{" + "RazonSocial=" + RazonSocial + ", NIT=" + NIT + ", casas=" + casas + '}';
    }

}   // Fin de la clase EmpresaCondominios
