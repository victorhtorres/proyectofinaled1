
package tf_edya1_2016_03_apellidos;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 * Clase para pedir y validar datos ingresados por teclado.
 *
 * @author Víctor H. Torres
 * @version 1.0.1
 * @see https://github.com/victorhtorres/Utilitaria
 *
 */
public final class Util {

    /**
     * Método que captura una cadena y lo convierte en un número entero.
     *
     * @param msg es el mensaje a mostrar en pantalla para pedir los datos.
     * @exception NumberFormatException
     * @return int
     */
    @SuppressWarnings("InfiniteRecursion")
    public static int leerEntero(String msg) {
        int entero;
        try {
            entero = Integer.parseInt(JOptionPane.showInputDialog(msg));
        } catch (NumberFormatException e) {
            System.out.println("Excepción: " + e.toString());
            JOptionPane.showMessageDialog(null, "Se ha presentado un error:"
                                          + "\n- Se ha ingresado un valor que no es numérico o no es un entero."
                                          + "\n- Por favor, intente de nuevo.",
                                          "Error de excepción.", JOptionPane.ERROR_MESSAGE);
            entero = leerEntero(msg);
        }
        return entero;
    }

    /**
     * Método que captura una cadena y lo convierte en un número real.
     *
     * @param msg es el mensaje a mostrar en pantalla para pedir los datos.
     * @exception NumberFormatException
     * @return double
     */
    @SuppressWarnings("InfiniteRecursion")
    public static double leerReal(String msg) {
        String cadena;
        @SuppressWarnings("UnusedAssignment")
        double real = 0;
        String[] arr;
        try {
            cadena = JOptionPane.showInputDialog(msg);
            // valida si la cadena tiene una coma y la cambio por punto decimal.
            if(cadena.contains(",")){
                arr = cadena.split(",");
                real = Double.parseDouble(arr[0]+"."+arr[1]);
            }else{
                real = Double.parseDouble(cadena);
            }
            
        } catch (NumberFormatException e) {
            System.out.println("Excepción: " + e.toString());
            JOptionPane.showMessageDialog(null, "Se ha presentado un error:"
                                        + "\n- Se ha ingresado un valor que no es numérico."
                                        + "\n- Por favor, intente de nuevo.",
                                        "Error de excepción.", JOptionPane.ERROR_MESSAGE);
            real = leerReal(msg);
        }
        return real;
    }

    /**
     * Método que captura una cadena y retorna el caracter inicial de la cadena.
     *
     * @param msg es el mensaje a mostrar en pantalla para pedir los datos.
     * @return char
     */
    @SuppressWarnings("InfiniteRecursion")
    public static char leerCaracter(String msg) {
        char c;
        String cadena;
        do {
            cadena = JOptionPane.showInputDialog(msg);
            if (cadena.length() == 0) {
                JOptionPane.showMessageDialog(null, "Error: No ha digitado ningún caracter."
                                                    + "\nPor favor, intente de nuevo.",
                                                    "Error", JOptionPane.ERROR_MESSAGE);
            }
        } while (cadena.length() == 0);

        c = cadena.charAt(0);

        return c;
    }

    /**
     * Método que recibe, captura y retorna una cadena.
     *
     * @param msg es el mensaje a mostrar en pantalla para pedir los datos.
     * @return String
     */
    public static String leerCadena(String msg) {
        String arg = JOptionPane.showInputDialog(msg);
        return arg;
    }

    /**
     * Método que recibe una cadena y lo imprime en pantalla.
     *
     * @param msg es el mensaje a mostrar en pantalla.
     * @param titulo es el título de la ventana.
     * @param icono tipo de icono a mostrar en la ventana.
     * Algunas opciones de iconos: ERROR_MESSAGE = 0, INFORMATION_MESSAGE = 1, 
     * WARNING_MESSAGE = 2, QUESTION_MESSAGE = 3, PLAIN_MESSAGE = -1.
     * 
     */
    public static void imprimir(String msg, String titulo, int icono) {
        JOptionPane.showMessageDialog(null, msg, titulo, icono);
    }

    /**
     * Método que muestra un menú de opciones.
     *
     * Se debe instanciar dentro de este método, un objeto de alguna clase, que luego invoque los métodos
     * que desea utilizar en las opciones del menú (dentro del switch).
     */
    public static void mostrarMenu() {
        String msgError = "Error: ingrese una opción válida.";
        boolean continuar = true;
        String menu = "*** Menú principal ***\n\n"
                + "1 - Item 1.\n"
                + "2 - Item 2.\n"
                + "3 - Item 3.\n"
                + "4 - Salir.\n";
        
        while (continuar) {
            int opcion = leerEntero(menu);
            switch (opcion) {
                case 1: /* invoque un método aquí. */ System.out.println("Test item 1");
                    break;
                case 2: /* invoque un método aquí. */ System.out.println("Test item 2");
                    break;
                case 3: /* invoque un método aquí. */ System.out.println("Test item 3");
                    break;
                case 4:
                    continuar = false;
                    break;
                default: imprimir(msgError, "Error: Ha ingresado una opción inválida.", 0);

            }
        }

    }
    
    /**
     * Método que captura un valor entero por consola.
     *
     * @param msg es el mensaje a mostrar en consola.
     * @return int
     */
    @SuppressWarnings("InfiniteRecursion")
    public static int leerEnteroConsola(String msg){
        int entero = 0;
        Scanner entrada = new Scanner(System.in);
        
        try{
            System.out.print(msg);
            entrada.nextInt();
        }catch(NoSuchElementException e){
            System.out.println("Se ha presentado un error: "
                              + "\nSe ha ingresado un valor que no es numérico o no es un entero."
                              + "\nExcepción: " + e.toString()
                              + "\nPor favor, intente de nuevo.");
            entero = leerEnteroConsola(msg);
        }
        
        return entero;
    }

}
