
package tf_edya1_2016_03_apellidos;

/**
 * Fecha de asignación: Cali, 3 de noviembre de 2016.
 * Fecha y hora máximas de entrega: viernes 11 de noviembre a las 8:00 PM, 
 * vía Moodle. Recuerden enviar el Diseño UML y colocar apellidos, nombres y
 * códigos en sus entregas.
 * 
 * @author Apellidos, Nombres y Códigos.
 * 
 */

// Clase que define una casa del condominio:

public class Casa {
    // Atributos:
    private int numero;             // Número de la casa (entero positivo). 
    private String propietario;     // Nombre del propietario (cadena NO vacía).
    private double valorAdmon;      // Valor a pagar (> 0) por el rubro de Administración.  

    public Casa(int numero, String propietario, double valorAdmon) {
        this.numero = numero;
        this.propietario = propietario;
        this.valorAdmon = valorAdmon;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public double getValorAdmon() {
        return valorAdmon;
    }

    public void setValorAdmon(double valorAdmon) {
        this.valorAdmon = valorAdmon;
    }
        

    @Override
    public String toString() {
        return "Casa{" + "numero=" + numero + ", propietario=" + propietario + ", valorAdmon=" + valorAdmon + '}';
    }
    
    
    
}   // Fin de la clase Casa