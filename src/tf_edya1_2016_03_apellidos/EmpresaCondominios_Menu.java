package tf_edya1_2016_03_apellidos;

import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 * Fecha de asignación: Cali, 3 de noviembre de 2016. Fecha y hora máximas de
 * entrega: viernes 11 de noviembre a las 8:00 PM, vía Moodle. Recuerden enviar
 * el Diseño UML y colocar apellidos, nombres y códigos en sus entregas.
 *
 * @author Apellidos, Nombres y Códigos.
 *
 */
// Clase que define un menú, para ejecutar los requerimientos de la 
// empresa administradora de los condominios:
public class EmpresaCondominios_Menu {

    private String menu;
    private byte opcion;
    private final EmpresaCondominios obj = new EmpresaCondominios();

    /**
     * Constructor:
     */
    public EmpresaCondominios_Menu() {

        String razon;
        do {
            razon = Util.leerCadena("Ingrese el nombre de la razón social: ");

            if (razon.equals("") || razon.equals(" ")) {
                Util.imprimir("El nombre de la razón no puede estár vacío.", "Error entrada de datos.", 0);
            }

        } while (razon.equals("") || razon.equals(" "));

        obj.setRazonSocial(razon);

        setMenu();

        while (true) {         // Ciclo infinito.
            setOpcion();
            ejecutarMenu();
        }

    }

    // Getters:
    public String getMenu() {
        return menu;
    }

    public byte getOpcion() {
        return opcion;
    }

    // Setters:
    private void setMenu() {

        // Asignación del texto del menú en la variable menu:
        menu = "Menú de opciones\n\n"
                + "1 - Ingresar casa.\n"
                + "2 - Reporte de las casas en mora.\n"
                + "3 - Buscar una casa por su número.\n"
                + "4 - Total recaudo de las casas que están al día.\n"
                + "5 - La casa con mayor valor adeudado.\n"
                + "6 - Salir";
    }

    /**
     * Método privado que presenta el menú y selecciona una opción válida:
     */
    private void setOpcion() {
        boolean errorNFE;   // Si se lanza NumberFormatException, errorNFE = true
        do {
            try {
                do {
                    // Lectura y validación de la opción seleccionada:
                    opcion = Byte.parseByte(JOptionPane.showInputDialog(menu));
                    if (opcion < 1 || opcion > 17) {
                        System.out.println(opcion + " es una opción inválida.");
                        JOptionPane.showMessageDialog(null, opcion + " es una "
                                + "opción inválida.");
                    }
                } while (opcion < 1 || opcion > 17);	// 1 <= opcion <= 17
                errorNFE = false;
            } catch (NumberFormatException nfe) {
                errorNFE = true;
                System.out.println("Error: " + nfe);
                JOptionPane.showMessageDialog(null, "Error: " + nfe,
                        "Excepción NumberFormatException", 0);
            }
        } while (errorNFE);

    }   // Fin del método setOpcion( )

    /**
     * Método privado que ejecuta los requerimientos de la empresa según la
     * opción seleccionada:
     */
    private void ejecutarMenu() {

        switch (opcion) {
            case 1: {
                int num;
                String propietario;
                double vPagar;
                do {
                    num = Util.leerEntero("Ingrese el número de la casa:");

                    if (num <= 0) {
                        Util.imprimir("El número de la casa debe ser mayor a cero.", "Error entrada de dato.", 0);
                    }

                } while (num <= 0);

                do {
                    propietario = Util.leerCadena("Ingrese el nombre del propietario:");
                    if (propietario.equals("") || propietario.equals(" ")) {
                        Util.imprimir("El nombre del propietario no puede estár vacío.", "Error entrada de datos.", 0);
                    }
                } while (propietario.equals("") || propietario.equals(" "));

                do {
                    vPagar = Util.leerReal("Ingrese valor a pagar:");

                    if (vPagar <= 0) {
                        Util.imprimir("El valor a pagar no puede ser menor o igual a cero.", "Error entrada de dato.", 0);
                    }
                } while (vPagar <= 0);

                obj.setCasas(new Casa(num, propietario, vPagar));
                System.out.println("Se ha agregado la siguiente casa:\n" + obj.getCasas().getLast().toString());

            }
            break;
            case 2: {

                if (obj.getCasas().size() == 0) {
                    Util.imprimir("No se tiene casas en la base de datos aún.", "¡Información Importante!", 1);
                } else {
                    obj.reporteCasasMora();
                }

            }

            break;
            case 3: {
                int buscarCasa;
                int encontrado;

                if (obj.getCasas().size() == 0) {
                    Util.imprimir("No se tiene casas en la base de datos aún.", "¡Información Importante!", 1);
                } else {
                    do {
                        buscarCasa = Util.leerEntero("Ingrese el número de la casa:");
                        if (buscarCasa <= 0) {
                            Util.imprimir("El número de la casa debe ser mayor a cero.", "Error entrada de dato.", 0);
                        }
                    } while (buscarCasa <= 0);

                    encontrado = obj.busquedaBinariaRecursiva(buscarCasa);

                    if (encontrado == -1) {
                        Util.imprimir("No se encontró una casa con ese número.", "¡Información Importante!", 1);
                    } else {
                        System.out.println("Casa encontrada:\n"
                                + obj.getCasas().get(encontrado).toString());
                    }
                }

            }

            break;
            case 4: {

                if (obj.getCasas().size() == 0) {
                    Util.imprimir("No se tiene casas en la base de datos aún.", "¡Información Importante!", 1);

                } else {
                    obj.totalRecaudadoCasasOk();
                }

            }

            break;
            case 5: {

                if (obj.getCasas().size() == 0) {
                    Util.imprimir("No hay casas en la base de datos aún.", "¡Información Importante!", 1);
                } else {
                    obj.mayorValorAdeudado();
                }

            }

            break;
            case 6: {
                terminar();
            }

            break;

        }
    }

    private void terminar() {
        String msg = "\nUsted ha seleccionado Terminar\n";
        System.out.println(msg);
        JOptionPane.showMessageDialog(null, msg, "TERMINAR",
                JOptionPane.PLAIN_MESSAGE);
        System.exit(0);
    }

}
